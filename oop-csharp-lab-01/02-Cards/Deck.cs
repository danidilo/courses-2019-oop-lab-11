﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Card this[ItalianSeed seed, ItalianValue v]
        {
            get {
                foreach(Card c in cards)
                {
                    if(c.Seed.Equals(seed.ToString()) && c.Value.Equals(v.ToString()))
                    {
                        return c;
                    }
                }
                throw new NotImplementedException();
            }
            set {

            }
        }

        public Deck()
        { 
        }

        public void Initialize()
        {
            int cont = 0;
            cards = new Card[40];
            foreach(ItalianSeed s in (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed)))
            {
                foreach (ItalianValue v in (ItalianValue[])Enum.GetValues(typeof(ItalianValue)))
                {
                    if(cont >= 40)
                    {
                        break;
                    }
                    cards[cont] = new Card(Convert.ToString(v), Convert.ToString(s));
                    cont++;
                }
            }
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
             for(int i = 0; i < 40; i++)
            {
                Console.WriteLine(cards[i].ToString());
            }
        }

    }

    class FrenchDeck
    {
        private Card[] cards;

        public Card this[FrenchSeed seed, FrenchValue v]
        {
            get
            {
                foreach (Card c in cards)
                {
                    if (c.Seed.Equals(seed.ToString()) && c.Value.Equals(v.ToString()))
                    {
                        return c;
                    }
                }
                throw new NotImplementedException();
            }
            set
            {

            }
        }

        public FrenchDeck()
        {
        }

        public void Initialize()
        {
            int cont = 0;
            cards = new Card[54];
            foreach (FrenchSeed s in (FrenchSeed[])Enum.GetValues(typeof(FrenchSeed)))
            {
                foreach (FrenchValue v in (FrenchValue[])Enum.GetValues(typeof(FrenchValue)))
                {
                    if (cont >= 54)
                    {
                        break;
                    }
                    cards[cont] = new Card(Convert.ToString(v), Convert.ToString(s));
                    cont++;
                }
            }
            cards[cont++] = new Card("JOLLY", "JOLLY");
            cards[cont++] = new Card("JOLLY", "JOLLY");
        }

        public void Print()
        {
            for (int i = 0; i < 40; i++)
            {
                Console.WriteLine(cards[i].ToString());
            }
        }

    }

    enum ItalianSeed
    {
        DENARI = 0,
        COPPE = 1,
        SPADE = 2,
        BASTONI = 3
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }

    enum FrenchSeed
    {
        QUADRI,
        CUORI,
        PICCHE,
        FIORI
    }
    enum FrenchValue
    {
        UNO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        OTTO,
        NOVE,
        J,
        Q,
        K
    }
}
