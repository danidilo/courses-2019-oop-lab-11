﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComplexNumbers
{
    public static class InvertExtension
    {
        public static ComplexNum Invert(this ComplexNum num)
        {
            return new ComplexNum(num.Re / (Math.Pow(num.Re, 2) + Math.Pow(num.Im, 2)), -num.Im / (Math.Pow(num.Re, 2) + Math.Pow(num.Im, 2)));
        }
    }
}
